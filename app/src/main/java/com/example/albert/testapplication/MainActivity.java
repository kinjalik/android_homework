package com.example.albert.testapplication;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button acceptBtn = findViewById(R.id.accept_btn);

        final EditText nameBar = findViewById(R.id.edit_name);


        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_welcome);
                TextView user_name = findViewById(R.id.meatbag_welcome);


                Editable name = nameBar.getText();

                String msg = String.format(getString(R.string.meatbag_welcome_msg), name);

                user_name.setText(msg);


                Button returnBtn = findViewById(R.id.return_btn);
                returnBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setContentView(R.layout.activity_main);
                        nameBar.setText("");
                    }
                });

            }
        });


    }
}
